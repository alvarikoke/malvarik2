from django.db import models

class Categoria(models.Model):
	nombre = models.CharField(max_length=144)
	creacion = models.DateTimeField(auto_now_add=True)
	

	def __str__(self):
		return self.nombre


class Noticia(models.Model):
	titulo = models.CharField(max_length=144)
	cuerpo = models.TextField()
	fecha_publicacion = models.DateField()
	destacada = models.BooleanField(default=False)
	categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
	imagen = models.ImageField(upload_to='imagenes')

	def __str__(self):
		return self.titulo

