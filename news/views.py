from django.shortcuts import render
from news.models import Noticia
from django.http import HttpResponse, Http404


def inicio(request):
	nombre_vista = 'index.html'
	data = {}
	#SELECT * FROM app WHERE status = 1
	data['publicacion'] = Noticia.objects.order_by('-fecha_publicacion')
	data['destacada'] = Noticia.objects.filter(destacada=True).order_by('-fecha_publicacion')[:1]

	return render(request, nombre_vista, data)


def dato(request, id_noticia):
	try:
		muro = Noticia.objects.get(pk=id_noticia)
	except Noticia.DoesNotExist:
		raise Http404("Noticia Inexistente")

	nombre_vista= 'noticia_detalle.html'
	# data = {}
	return render(request, nombre_vista, {'news':muro})


