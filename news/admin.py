from django.contrib import admin
from news.models import Categoria, Noticia



@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
	pass

@admin.register(Noticia)
class NoticiaAdmin(admin.ModelAdmin):
	list_display = (
		'titulo',
		'cuerpo',
		'fecha_publicacion',
		'categoria',

	)

# Register your models here.
