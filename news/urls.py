from django.urls import path
from news import views

urlpatterns = [
    path('', views.inicio, name='inicio'),
    path('dato/<int:id_noticia>', views.dato, name='detalle_noticia'),
]